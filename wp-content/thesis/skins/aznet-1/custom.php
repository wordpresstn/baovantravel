<?php
/**
 * Name: Custom class file.
 * Description: Custome hock and function my theme aznet 1.
 * User: Hoang Neo
 * Date: 16/11/2017
 */

if (!class_exists('CustomThemeThesis')) :

    class CustomThemeThesis
    {
        public function __construct()
        {
            global $MySkinFolder;
            $MySkinFolder = "aznet-1";
            /**
             * load includes
             */
            require(__DIR__. '/includes/index.php');

            /**
            * Call templates
            */
            require(__DIR__. '/templates/autoload.php');
        }
    }

    /**
     *check class is exit
     * call class
     */

    function HN_CustomThemeThesis()
    {
        global $HNCustomThemeThesis;

        if (!isset($HNCustomThemeThesis)) {
            $HNCustomThemeThesis = new CustomThemeThesis();
        }

        return $HNCustomThemeThesis;
    }

    HN_CustomThemeThesis();

endif;