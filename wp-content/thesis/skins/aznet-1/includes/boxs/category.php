<?php
/**
 * Created by PhpStorm.
 * User: hoang
 */

if(!function_exists('az_box_list_taxonomy')):
    function az_box_list_taxonomy()
    {
        $terms = get_terms( array(
            'taxonomy' => 'product_cat',
            'hide_empty' => false,
        ) );
        if( !empty($terms) ):
            $str = '<h2 class="title-product">Sản phẩm</h2>';
            $str .= '<ul class="product_cat row">';
            foreach ($terms as $term)
            {
                // get the thumbnail id using the queried category term_id
                $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
                // get the image URL
                $image = wp_get_attachment_url( $thumbnail_id );

                $str .= '<li class="col s6 m3">';
                $str .= '<a href="'.get_term_link($term->term_id).'">';
                $str .= '<img src="'.$image.'">';
                $str .= '<h2>'.$term->name.'</h2>';
                $str .= '</a>';
                $str .= '</li>';
            }
            $str .= '</ul>';
            echo  $str;
        endif;
    }
endif;

 if(!function_exists('az_box_list_tour_local')):
    function az_box_list_tour_local()
    {
        
        $args = array(
            'posts_per_page' => 30,
            'offset'         => 0,
            'post_type' => 'trip',
             'meta_query'    => array(
                                        array(
                                            'key'   => 'trip_kiutour',
                                            'value' => 'trongnuoc',          
                                        ),
            'tax_query' => array(
                array(
                'taxonomy' => 'destination'   )
              )
        ));
        $postslist = get_posts( $args );
        if ( $postslist ) {
            $str ='' ;
            foreach ( $postslist as $posts ) {
				$star = get_field('trip_hotel', $posts->ID);
				// khách san //
				$tem = '';
				// Điểm khởi hành //
				$des = '';
				for($i=0 ; $i < $star  ; $i++){
					$tem .= '<span><i class="fas fa-star"></i></span>'; 
				}

				$terns = get_the_terms(  $posts->ID, 'destination' ) ; 
                if(!empty($terns)){
                    foreach($terns as $tern){ 
                        $des .= $tern->name .' ' ; 
                    } 
                }
                $str .= ' <div class="box-tour">
                            <a href="'.get_permalink($posts->ID).'"><img src=" '.get_the_post_thumbnail_url($posts->ID,'full').'" class="img-responsive"  /></a>
                            <div class="info">
                                <h5><a href="'.get_permalink($posts->ID).'" >'.$posts->post_title.'</a></h5>
                                <p class="price-tour">'.  get_field('trip_giakm', $posts->ID) .' VND</p>
                                <div class="info-more" >
                                    <p class="info-time">Thời gian: '.get_field('trip_thoigian', $posts->ID ).'</p>
                                    <p class="info-begin">Điểm khởi hành: '.$des.'  </p>
                                    <p class="info-bus">Phương tiện: '.get_field('trip_phuongtien', $posts->ID).'</p>
                                    <p class="info-hotel">Khách sạn: '. $tem .'  </p>
                                    <button class="btn pull-left  btn-danger"><a href="/dat-tour/?tourid='.$posts->ID.'  ">Đặt ngay</a></button>
                                    <button class="btn pull-right  btn-info" ><a href="'.get_permalink($posts->ID).'">Chi tiết</a></button>
                                </div>
                            </div>
                        </div>';

            }
            echo $str;
        }
    }
endif;


  if(!function_exists('az_box_list_tour_word')):
    function az_box_list_tour_word()
    {
       
        $args = array(
            'posts_per_page' => 30,
            'offset'         => 0,
            'post_type' => 'trip',
            'meta_query'    => array(
                                        array(
                                            'key'   => 'trip_kiutour',
                                            'value' => 'nuocngoai',          
                                        ),
            'tax_query' => array(
                array(
					'taxonomy' => 'destination' 
                )
              )
        ));
        $postslist = get_posts( $args );
        if ( $postslist ) {
            $str ='' ;
            foreach ( $postslist as $posts ) {
				$star = get_field('trip_hotel', $posts->ID);
				// Sao //
				$tem = '';
				// Điểm khởi hành //
				$des = '';
				for($i=0 ; $i < $star  ; $i++){
					$tem .= '<span><i class="fas fa-star"></i></span>'; 
				}
				$terns = get_the_terms(  $posts->ID, 'destination' ) ; 
                if(!empty($terns)){
                    foreach($terns as $tern){ 
                        $des .= $tern->name .' ' ; 
                    } 
                }
                 
                $str .= ' <div class="box-tour">
                            <a href="'.get_permalink($posts->ID).'"><img src=" '.get_the_post_thumbnail_url($posts->ID,'full').'" class="img-responsive"  /></a>
                            <div class="info">
                                <h5><a href="'.get_permalink($posts->ID).'" >'.$posts->post_title.'</a></h5>
                                <p class="price-tour">'.  get_field('trip_giakm', $posts->ID) .' VND</p>
                                <div class="info-more" >
                                    <p class="info-time">Thời gian: '.get_field('trip_thoigian', $posts->ID ).'</p>
                                    <p class="info-begin">Điểm khởi hành: '.$des.'  </p>
                                    <p class="info-bus">Phương tiện: '.get_field('trip_phuongtien', $posts->ID).'</p>
                                    <p class="info-hotel">Khách sạn: '. $tem .'  </p>
                                    <button class="btn pull-left  btn-danger"><a href="/dat-tour/?tourid='.$posts->ID.'  ">Đặt ngay</a></button>
                                    <button class="btn pull-right  btn-info" ><a href="'.get_permalink($posts->ID).'">Chi tiết</a></button>
                                </div>
                            </div>
                        </div>';

            }
            echo $str;
        }
    }
endif;
 

/// slider tin tức
if(!function_exists('az_box_list_news_1')):
    function az_box_list_news_1()
    {
        $option_value = 64 ;
        $args = array(
            'posts_per_page' => 1,
            'numberposts' =>1,
            'offset'         => 0,
            'category'       => $option_value
        );
        $postslist = get_posts( $args );
        if ( $postslist ) {
            $str = '';
            foreach ( $postslist as $posts ) {
                $str .= '   <div class="box-news" >
                                <a href="'.get_permalink($posts->ID).'" tabindex="0"><img src="'.get_the_post_thumbnail_url( $posts->ID, 'medium' ).'" class="img-responsive" style="width:100%;"></a>
                                <div class="info">
                                    <h5><a href="'.get_permalink($posts->ID).'" tabindex="0"> '.$posts->post_title.' </a></h5>
                                    <p class="description">'. $posts->post_excerpt .' </p>
                                </div>
                            </div> ';
            }
            
            echo $str;
        }
    }
endif;

if(!function_exists('az_box_list_news')):
    function az_box_list_news()
    {
        $option_value = 64 ;
        $args = array(
            'posts_per_page' => 6,
            'offset'         => 1,
            'category'       => $option_value
        );
        $postslist = get_posts( $args );
        if ( $postslist ) {
      
            foreach ( $postslist as $posts ) {
                 $str .= '<div class="content-slide-new">
                            <img src="'.get_the_post_thumbnail_url( $posts->ID, 'thumbnail' ).'" alt=""  class="img-responsive" > 
                            <div class="des">
                               <h6><a href="'.get_permalink($posts->ID).'"> '.$posts->post_title.'</a></h6>
                               <p> </p>
                            </div>
                         </div>';
            }
            
            echo $str;
        }
    }
endif;
// end slide tin tức


if(!function_exists('az_box_list_camnhan')):
    function az_box_list_camnhan()
    { 
        $args = array(
            'posts_per_page' => 50,
            'offset'         => 0,
            'post_type'       => 'cam_nhan_khach_hang'
        );
        $postslist = get_posts( $args );
        if ( $postslist ) {
             $str = '';
            foreach ( $postslist as $posts ) {
                $str .= '<div class="row-fell">
                            <div class="abs abs-top"><b class=" "><img src="/images/quote_1.png" ></b></div>
                            <div class="col3slick ">
                                <img src="'.get_the_post_thumbnail_url( $posts->ID, 'medium' ).'">
                            </div>
                            <div class=" col9slick" >
                                 <p> '. $posts->post_excerpt .' </p>
                                 <p class="text-center"><span>Khách hàng: '. $posts->post_title .'</span></p>
                            </div>
                            <div  class="abs abs-bottom"><img src="/images/quote_2.png" ></div>  
                        </div>
                ';
            }
            echo $str;
        }
    }
endif;