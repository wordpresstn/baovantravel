<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 16/12/2017
 * Time: 15:13
 */

if(!function_exists('az_box_send_email')):
    function az_box_send_email()
    {
        if (is_active_sidebar('send-email')) :
            dynamic_sidebar('send-email');
        endif;
    }
endif;

if(!function_exists('az_box_social_network')):
    function az_box_social_network()
    {
        if (is_active_sidebar('social-network')) :
            dynamic_sidebar('social-network');
        endif;
    }
endif;

if(!function_exists('az_box_footer')):
    function az_box_footer()
    {
        if (is_active_sidebar('footer')) :
            dynamic_sidebar('footer');
        endif;
    }
endif;

if(!function_exists('az_box_footer_copyright')):
    function az_box_footer_copyright()
    {
        if (is_active_sidebar('copyright')) :
            dynamic_sidebar('copyright');
        endif;
    }
endif;