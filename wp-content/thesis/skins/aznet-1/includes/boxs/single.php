<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 12/28/2017
 * Time: 2:35 PM
 */

if(!function_exists('az_box_description_single_product')):
    function az_box_description_single_product()
    {
        if (is_active_sidebar('description-single-product')) :
            dynamic_sidebar('description-single-product');
        endif;
    }
endif;