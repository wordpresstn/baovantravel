<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 18/12/2017
 * Time: 23:23
 */

if(!function_exists('az_box_info_top')):
    function az_box_info_top()
    {
        if (is_active_sidebar('info-1')) :
            dynamic_sidebar('info-1');
        endif;
    }
endif;