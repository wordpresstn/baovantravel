<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 */

if(!function_exists('az_box_menu_primary')):
    function az_box_menu_primary()
    {
       
            wp_nav_menu(array(
                'menu' => 'main-menu',
                'container' => false,
                'theme_location' => 'main-menu',
                'menu_class' => 'menu',
                'menu_id' => 'main-menu',
            ));
        
    }
endif;

if(!function_exists('az_box_menu_primary_1')):
    function az_box_menu_primary_1()
    {
        if (has_nav_menu('primary_1')) :
            wp_nav_menu(array(
                'container' => false,
                'theme_location' => 'primary_1',
                'menu_class' => 'menu',
                'menu_id' => 'main-menu-page-1',
            ));
        endif;
    }
endif;

if(!function_exists('az_box_menu_primary_2')):
    function az_box_menu_primary_2()
    {
        if (has_nav_menu('primary_2')) :
            wp_nav_menu(array(
                'container' => false,
                'theme_location' => 'primary_2',
                'menu_class' => 'menu',
                'menu_id' => 'main-menu-page-2',
            ));
        endif;
    }
endif;

if(!function_exists('az_box_menu_primary_mobile')):
    function az_box_menu_primary_mobile()
    {
        if (has_nav_menu('primary_mobile')) :
            wp_nav_menu(array(
                'container' => false,
                'theme_location' => 'primary_mobile',
                'menu_class' => 'side-nav primary_mobile multilevel-accordion-menu vertical',
                'menu_id' => 'nav-mobile',
            ));
        endif;
    }
endif;

if(!function_exists('az_box_menu_category')):
    function az_box_menu_category()
    {
        if (is_active_sidebar('menu-category')) :
            dynamic_sidebar('menu-category');
        endif;
    }
endif;