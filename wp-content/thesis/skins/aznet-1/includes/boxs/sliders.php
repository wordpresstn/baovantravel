<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 09/12/2017
 * Time: 17:40
 */

if(!function_exists('az_box_slider_home')):
    function az_box_slider_home()
    {
       echo do_shortcode('[metaslider id="539"]'); 
    }
endif;

if(!function_exists('az_box_images_home')):
    function az_box_images_home()
    {
        if (is_active_sidebar('images-home')) :
            dynamic_sidebar('images-home');
        endif;
    }
endif;

if(!function_exists('az_box_slider_page')):
    function az_box_slider_page()
    {
        if (is_active_sidebar('slider-page')) :
            dynamic_sidebar('slider-page');
        endif;
    }
endif;