<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 */

/**
 * add library
 */

// if(!function_exists('az_addLibraryJs')):
// function az_addLibraryJs()
// {
//     global $MySkinFolder;
//     echo '<script type=\'text/javascript\' src=\'' . get_home_url() . '/wp-content/thesis/skins/'.$MySkinFolder.'/public/foundation/foundation.min.js?ver=0.1\'></script>';
//     echo '<script type=\'text/javascript\' src=\'' . get_home_url() . '/wp-content/thesis/skins/'.$MySkinFolder.'/public/skin.js?ver=0.0.1\'></script >
// ';
// }
// add_action('wp_footer', 'az_addLibraryJs');
// endif;

/**
 * add library
 */
 if(!function_exists('az_wp_enqueue_script')):
    function az_wp_enqueue_script() {



        //add style
        wp_enqueue_style( 'bootstrap-css', home_url(). '/assets/css/bootstrap.min.css' );
        wp_enqueue_style( 'fontawesome-css', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css' );
        wp_enqueue_style( 'date-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
        wp_enqueue_style( 'font-goole-css', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=latin-ext,vietnamese' );
         wp_enqueue_style( 'slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
        wp_enqueue_style( 'mmenu-css', home_url(). '/assets/css/jquery.mmenu.css' );

        wp_enqueue_style( 'style-css', home_url(). '/assets/css/style.css' );

        if(is_page('dat-tour')) {
            wp_enqueue_style('booking', home_url() . '/assets/css/booking.css');
            wp_enqueue_script( 'bookingjs',home_url(). '/assets/js/booking.js', array(), '1.0.0', true );
        }


        //add script
        wp_enqueue_script( 'maps-api-js','https://maps.googleapis.com/maps/api/js?key=AIzaSyA-0ChMOHPk4zy18EsyxnWwGzWGRS7cFKo', array(), '1.0.0', true );

        // date picker
         wp_enqueue_script( 'date-api-js','https://code.jquery.com/ui/1.12.1/jquery-ui.js', array(), '1.12.1', false );
        // end date picker

        wp_enqueue_script( 'mmenu-js',home_url(). '/assets/js/jquery.mmenu.js', array(), '1.0.0', true );

        wp_enqueue_script( 'bootstrap-js', home_url(). '/assets/js/bootstrap.js  ', array(), '3.0.0', true );
        wp_enqueue_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js' , array(), '1.9.0', true );

         
        wp_enqueue_script( 'common-js',home_url(). '/assets/js/common.js', array(), '1.0.0', true );
        
    }
add_action( 'wp_enqueue_scripts', 'az_wp_enqueue_script' );
endif;
add_theme_support( 'post-thumbnails' );
add_filter( 'get_pagenum_link', 'user_trailingslashit' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Cài đặt website',
		'menu_title'	=> 'Cài đặt website',
		'menu_slug' 	=> 'website-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyA-0ChMOHPk4zy18EsyxnWwGzWGRS7cFKo');
}

add_action('acf/init', 'my_acf_init');

/**
 * add library
 */
// if(!function_exists('az_addStyleSkin')):
// function az_addStyleSkin()
// {
//     global $MySkinFolder;
//     echo '<link rel="stylesheet" type="text/css" href=\'' . get_home_url() . '/wp-content/thesis/skins/'.$MySkinFolder.'/public/style.css?ver=0.0.1\' />';
// }
// add_action('wp_head', 'az_addStyleSkin', 90);
// endif;