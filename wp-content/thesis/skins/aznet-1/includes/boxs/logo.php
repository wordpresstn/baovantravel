<?php
/**
 * Created by PhpStorm.
 * User: hoang
 */

if(!function_exists('az_box_logo_primary')):
    function az_box_logo_primary()
    {
        $text = $logo = '';
        global $thesis;
        if (isset($thesis->skin->logo->image['src']) && !empty($thesis->skin->logo->image['src']))
        {
            $logo = '<img src="'.$thesis->skin->logo->image['src'].'" alt="'.get_bloginfo( 'name' ).'" />';
        }else{
            $logo = get_bloginfo( 'name' );
        }


        if (is_home())
        {
            $text .= '<h1 class="title-site"><a href="'.get_home_url().'" title="'.get_bloginfo( 'name' ).'">';
            $text .= $logo;
            $text .= '</a></h1>';
        }else{
            $text .= '<div class="title-site"><a href="'.get_home_url().'" title="'.get_bloginfo( 'name' ).'">';
            $text .= $logo;
            $text .= '</a></div>';
        }
        echo  $text;
    }
endif;