<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 12/25/2017
 * Time: 4:11 PM
 */


if(!function_exists('az_box_top_site')):
    function az_box_top_site()
    {
        if (is_active_sidebar('top_site')) :
            dynamic_sidebar('top_site');
        endif;
    }
endif;