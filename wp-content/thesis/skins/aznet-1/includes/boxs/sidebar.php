<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 10/12/2017
 * Time: 23:08
 */

if(!function_exists('az_box_sitebar_site')):
    function az_box_sitebar_site()
    {
        if (is_active_sidebar('sidebar')) :
            dynamic_sidebar('sidebar');
        endif;
    }
endif;