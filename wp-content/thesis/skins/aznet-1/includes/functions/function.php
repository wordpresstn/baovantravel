<?php

/**
 * Name: function file.
 * Description: list function for skin.
 * User: Hoang Neo
 * Date: 16/11/2017
 */

if (!function_exists('az_get_excerpt_content')):
    function az_get_excerpt_content($id, $limit = 40) {

        $content_post = get_post($id);

        $content = explode(' ', $content_post->post_excerpt, $limit);
        if (count($content)>=$limit) {
            array_pop($content);
            $content = implode(" ",$content);
        } else {
            $content = implode(" ",$content);
        }
        $content = preg_replace('/[.+]/','', $content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = str_replace("&nbsp;", '', $content);
        $content = strip_tags($content);
        return $content;
    }
endif;

if (!function_exists('az_get_price_product')):
    function az_get_price_product($price)
    {
        $number = number_format($price, 0, ',', '.');
        return $number;
    }
endif;

function my_search_filter($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $query->set('post_type', array( 'trip' ) );
      /**** filter tax****/
      $diemkhoihanh = $_GET['diemkhoihanh'] != '' ? $_GET['diemkhoihanh'] : '';
      if($diemkhoihanh != '0'){
         $taxquery = array(
                array(
                    'taxonomy' => 'diem_khoi_hanh',
                    'field' => 'id',
                    'terms' => $diemkhoihanh ,
                    'operator'=> 'IN'
                )
            );
            $query->set( 'tax_query', $taxquery );
      }
      $diemdich = $_GET['noiden'] != '' ? $_GET['noiden'] : '';
      if( $diemdich != '0' ){
         $taxquery = array(
                array(
                    'taxonomy' => 'destination',
                    'field' => 'id',
                    'terms' => $diemdich ,
                    'operator'=> 'IN'
                )
            );
            $query->set( 'tax_query', $taxquery );
      }
      $ngaykhoihanh = $_GET['ngaykhoihanh'] != '' ? $_GET['ngaykhoihanh'] : '';
      if ( $ngaykhoihanh != '' ) {
            $date = date("d/m/Y", strtotime($ngaykhoihanh));
            $metaquery = array(
                array(
                    'key' => 'trip_date_begin',
                    'value' =>  $date ,
                    'compare' => '=',
                    'type' => 'DATE'
                )           
            );
            $query->set( 'meta_query', $metaquery );   
      }
      $dongtour = $_GET['dongtour'] != '' ? $_GET['dongtour'] : '';
      if ( $dongtour != 'all' ) {
            $metaquery = array(
                array(
                    'key' => 'trip_loaitour',
                    'value' =>  $dongtour ,
                    'compare' => '=',
                    'type' => 'string'
                )           
            );
            $query->set( 'meta_query', $metaquery );   
      }
      $tourtype = $_GET['tourtype'] != '' ? $_GET['tourtype'] : '';
      if ( $tourtype != '' ) {
            $metaquery = array(
                array(
                    'key' => 'trip_kiutour',
                    'value' =>  $tourtype ,
                    'compare' => '=',
                    'type' => 'string'
                )           
            );
            $query->set( 'meta_query', $metaquery );   
      }
      $stock = $_GET['stock'] != '' ? $_GET['stock'] : '';
      if ( $stock != 'all' ) {
            $stock = intval($stock ) ; 
            if( $stock == 1 )
                $compare = '>';
            else 
                $compare = '=';
            $metaquery = array(
                array(
                    'key' => 'trip_socho',
                    'value' =>  0 ,
                    'compare' => $compare ,
                    'type' => 'numeric'
                )
            );
            $query->set( 'meta_query', $metaquery );   
      }
      $price = $_GET['price'] != '' ? $_GET['price'] : '';
      $price = intval($price) ;
      if ( $price != 0 ) {
            if($price == 1 ){
                $begin = 0 ; 
                $end   = 1000000 ; 
            }
            if($price == 2 ){
                $begin = 1000000 ; 
                $end   = 2000000 ; 
            }
            if($price == 3 ){
                $begin = 2000000 ; 
                $end   = 5000000 ; 
            }
            if($price == 4 ){
                $begin = 5000000 ; 
                $end   = 10000000 ; 
            }
            if($price == 5 ){
                $begin = 10000000 ; 
                $end   = 15000000 ; 
            }
            if($price == 6 ){
                $begin = 15000000 ; 
                $end   = 20000000 ; 
            }
            if($price == 7 ){
                $begin = 20000000 ; 
                $end   = 900000000 ; 
            }
            $metaquery = array(
                array(
                    'key' => 'trip_giakm',
                    'value' =>  array($begin,$end)  ,
                    'compare' => 'BETWEEN',
                    'type' => 'numeric'
                )           
            );
            $query->set( 'meta_query', $metaquery );   
      }
    }

  }
}
add_action('pre_get_posts','my_search_filter');