<?php

/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 17/11/2016
 * Time: 3:11
 */

if (!class_exists('RegisterWp')):
    class RegisterWp
    {
        public function __construct()
        {
            /**
             * dang ky methot wp
             */
            //$this->hookMenu();
            //add_action( 'widgets_init', array($this, 'customWidgetsInit') );
            //add_action( 'admin_menu', array($this, 'my_plugin_menu') );
            //add_action( 'admin_init', array($this, 'register_skin_option_settings') );
            //add_action( 'init', array($this, 'create_box_post_type'), 0);
        }
        /**
         * create new hook menu
         * Primary Menu and Social Links Menu
         */
        public function hookMenu()
        {
            // This theme uses wp_nav_menu() in two locations.
            register_nav_menus( array(
                'primary_1' => __( 'Primary Menu 1', 'customtheme' ),
                'primary_2' => __( 'Primary Menu 2', 'customtheme' ),
                'primary_mobile' => __( 'Primary Menu Mobile', 'customtheme' ),
                'isocial' => __( 'Menu isocial', 'customtheme' ),
                'top_site'  => __( 'Top menu', 'customtheme' ),
            ) );
        }
        /**
         * create widgets
         */
        public function customWidgetsInit()
        {
            register_sidebar( array(
                'name'          => __( 'Top Site', 'customtheme' ),
                'id'            => 'top_site',
                'description'   => __( 'Add top site.', 'customtheme' ),
                'before_widget' => '<div class="top-site-div small-8 medium-6 cell">',
                'after_widget'  => '</div>',
                'before_title'  => '',
                'after_title'   => '',
            ) );

            register_sidebar( array(
                'name'          => __( 'Slider Home', 'customtheme' ),
                'id'            => 'slider-home',
                'description'   => __( 'Add widgets here.', 'customtheme' ),
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '',
                'after_title'   => '',
            ) );

            register_sidebar( array(
                'name'          => __( 'Images Home', 'customtheme' ),
                'id'            => 'images-home',
                'description'   => __( 'Add widgets here.', 'customtheme' ),
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '',
                'after_title'   => '',
            ) );

            register_sidebar( array(
                'name'          => __( 'Menu Category', 'customtheme' ),
                'id'            => 'menu-category',
                'description'   => __( 'Add widgets here to appear in your footer.', 'customtheme' ),
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '<h3 class="title-menu-category">',
                'after_title'   => '</h3>',
            ) );

            register_sidebar( array(
                'name'          => __( 'Send Email', 'customtheme' ),
                'id'            => 'send-email',
                'description'   => __( 'Add widgets here.', 'customtheme' ),
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '<h3 class="title-widget">',
                'after_title'   => '</h3>',
            ) );

            register_sidebar( array(
                'name'          => __( 'Social Network', 'customtheme' ),
                'id'            => 'social-network',
                'description'   => __( 'Add widgets here.', 'customtheme' ),
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '<h3 class="title-widget">',
                'after_title'   => '</h3>   ',
            ) );

            register_sidebar( array(
                'name'          => __( 'Copyright', 'customtheme' ),
                'id'            => 'copyright',
                'description'   => __( 'Add widgets here.', 'customtheme' ),
                'before_widget' => '<div class="copy-right">',
                'after_widget'  => '</div>',
                'before_title'  => '',
                'after_title'   => '',
            ) );

            register_sidebar( array(
                'name'          => __( 'Footer', 'customtheme' ),
                'id'            => 'footer',
                'description'   => __( 'Add widgets here.', 'customtheme' ),
                'before_widget' => '<div id="%1$s" class="widget small-12 medium-3 cell %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );

            register_sidebar( array(
                'name'          => __( 'Description Single Product', 'customtheme' ),
                'id'            => 'description-single-product',
                'description'   => __( 'Add widgets here.', 'customtheme' ),
                'before_widget' => '<div class="description">',
                'after_widget'  => '</div>',
                'before_title'  => '',
                'after_title'   => '',
            ) );

            register_sidebar( array(
                'name'          => __( 'Sidebar', 'customtheme' ),
                'id'            => 'sidebar',
                'description'   => __( 'Add widgets here to appear in your footer.', 'customtheme' ),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );

        }

        public function my_plugin_menu() {
            add_options_page( 'Skin Options', 'Skin option', 'manage_options', 'my-unique-identifier', array($this, 'skinOptions') );
          
        }

        function register_skin_option_settings() {
            //register our settings
            register_setting( 'skin-option-settings', 'news_category' );
            register_setting( 'skin-option-settings', 'project_category' );
            register_setting( 'skin-option-settings', 'customer_category' );
            register_setting( 'skin-option-settings', 'service_category' );
            register_setting( 'skin-option-settings', 'product_category_home' );
        }

        public function skinOptions() {
            if ( !current_user_can( 'manage_options' ) )  {
                wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
            }
            echo '<div class="wrap">';
            echo '<h1>Skin option</h1>';
            echo '<p>Here is where the form would go if I actually had options.</p>';
            echo '<form method="post" action="options.php"> ';
            settings_fields( 'skin-option-settings' );
            do_settings_sections( 'skin-option-settings' );

            echo '<table class="form-table">';
            echo '<tr valign="top">';
            echo '<th scope="row">Id news category</th>';
            echo '<td><input type="number" name="news_category" value="'.esc_attr( get_option('news_category') ).'" /></td>';
            echo '</tr>';
            echo '<tr valign="top">';
            echo '<th scope="row">Id project category</th>';
            echo '<td><input type="number" name="project_category" value="'.esc_attr( get_option('project_category') ).'" /></td>';
            echo '</tr>';
            echo '<tr valign="top">';
            echo '<th scope="row">Id customer category</th>';
            echo '<td><input type="number" name="customer_category" value="'.esc_attr( get_option('customer_category') ).'" /></td>';
            echo '</tr>';
            echo '<tr valign="top">';
            echo '<th scope="row">Id service category</th>';
            echo '<td><input type="number" name="service_category" value="'.esc_attr( get_option('service_category') ).'" /></td>';
            echo '</tr>';
            echo '<tr valign="top">';
            echo '<th scope="row">Id category product home</th>';
            echo '<td><input type="text" name="product_category_home" value="'.esc_attr( get_option('product_category_home') ).'" /></td>';
            echo '</tr>';
            echo '</table>';
            submit_button();
            echo '</form></div>';
        }

        public function create_box_post_type()
        {
            $labels = array(
                'name'              => _x( 'Box type', 'taxonomy Box type name', 'skin-box-post' ),
                'singular_name'     => _x( 'Box type', 'taxonomy singular name', 'skin-box-post' ),
                'search_items'      => __( 'Search Box type', 'skin-box-post' ),
                'all_items'         => __( 'All Box type', 'skin-box-post' ),
                'parent_item'       => __( 'Parent Box type', 'skin-box-post' ),
                'parent_item_colon' => __( 'Parent Box type:', 'skin-box-post' ),
                'edit_item'         => __( 'Edit Box type', 'skin-box-post' ),
                'update_item'       => __( 'Update Box type', 'skin-box-post' ),
                'add_new_item'      => __( 'Add New Box type', 'skin-box-post' ),
                'new_item_name'     => __( 'New Box type Name', 'skin-box-post' ),
                'menu_name'         => __( 'Box type', 'skin-box-post' ),
            );

            $args = array(
                'has_archive'        => false,
                'hierarchical'       => true,
                'labels'            => $labels,
                'public'             => false,
                'publicly_queryable' => false,
                'show_ui'           => true,
                'show_admin_column' => true,
                'query_var'         => false,
                'rewrite'           => array( 'slug' => 'box-cat' ),
            );

            register_taxonomy( 'genre', array( 'box' ), $args );

            $labels = array(
                'name'               => _x( 'Box', 'post type general name', 'skin-box-post' ),
                'singular_name'      => _x( 'Box', 'post type singular name', 'skin-box-post' ),
                'menu_name'          => _x( 'Boxs', 'admin menu', 'skin-box-post' ),
                'name_admin_bar'     => _x( 'Box', 'add new on admin bar', 'skin-box-post' ),
                'add_new'            => _x( 'Add New', 'box', 'skin-box-post' ),
                'add_new_item'       => __( 'Add New box', 'skin-box-post' ),
                'new_item'           => __( 'New Box', 'skin-box-post' ),
                'edit_item'          => __( 'Edit Box', 'skin-box-post' ),
                'view_item'          => __( 'View Box', 'skin-box-post' ),
                'all_items'          => __( 'All Boxs', 'skin-box-post' ),
                'search_items'       => __( 'Search Boxs', 'skin-box-post' ),
                'parent_item_colon'  => __( 'Parent Boxs:', 'skin-box-post' ),
                'not_found'          => __( 'No boxs found.', 'skin-box-post' ),
                'not_found_in_trash' => __( 'No boxs found in Trash.', 'skin-box-post' )
            );

            $args = array(
                'labels'             => $labels,
                'description'        => __( 'Description.', 'skin-box-post' ),
                'public'             => false,
                'publicly_queryable' => false,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => false,
                'rewrite'            => array( 'slug' => 'post_box' ),
                'capability_type'    => 'post',
                'has_archive'        => false,
                'hierarchical'       => false,
                'menu_position'      => null,
                'supports'           => array( 'title', 'editor', 'author' )
            );

            register_post_type( 'box', $args );
        }

    }

    /**
     *check class is exit
     * call class
     * time: 16/11/2016
     * localtion: ho chi minh city
     */

    function registerwp()
    {
        global $registerwp;

        if (!isset($registerwp)) {
            $registerwp = new RegisterWp();
        }

        return $registerwp;
    }

    registerwp();
endif;