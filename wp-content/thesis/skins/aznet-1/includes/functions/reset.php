<?php
/**
 * Name: reset file.
 * Description: Reset default css and js wordpress.
 * User: Hoang Neo
 */

if (!class_exists('ReSetDefaultWP')):

    class ReSetDefaultWP
    {
        public function __construct()
        {
            remove_action('wp_head', 'wp_resource_hints', 2);
            // REMOVE WP EMOJI
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('wp_print_styles', 'print_emoji_styles');

            remove_action('admin_print_scripts', 'print_emoji_detection_script');
            remove_action('admin_print_styles', 'print_emoji_styles');

            add_action('wp_enqueue_scripts', array( $this, 'remove_default_scripts_plugin' ) );

            add_action('wp_footer', array($this, 'my_deregister_scripts'));

            add_action('wp_print_scripts', array($this, 'theme_slug_dequeue_footer_jquery'));

            add_action( 'pre_get_posts', array( $this, 'foo_modify_query_order' ) );

            add_filter( 'pre_get_posts', array( $this, 'prefix_limit_post_types_in_search' ) );

            add_filter( 'get_the_archive_title', array( $this, 'prefix_category_title') );

        }

        public function my_deregister_scripts()
        {
            wp_deregister_script('wp-embed');
        }

        /**
         * remove library Js
         */
        public function theme_slug_dequeue_footer_jquery()
        {
            if (!is_admin()) {
                //Remove Jquery default.
                //wp_deregister_script('jquery');
            }
        }


        /**
         * remove css pagenavi
         */
        public function remove_default_scripts_plugin(){
            wp_dequeue_style( 'wp-pagenavi' );
        }

        /**
         * order by date
         */
        public function foo_modify_query_order( $query ) {
            $query->set( 'orderby', 'date' );
            $query->set( 'order', 'DESC' );
        }
        /**
         * search post
         */
        public function prefix_limit_post_types_in_search( $query ) {
            if ( $query->is_search ) {
                $query->set( 'post_type', array( 'post' ) );
            }
            return $query;
        }

        public function prefix_category_title( $title ) {
            if ( is_category() || is_tax() ) {
                $title = single_cat_title( '', false );
            }
            if (is_archive()){
                return str_replace('Lưu trữ:', '', $title);
            }
            return $title;
        }
    }

    /**
     *check class is exit
     * call class
     */

    function HN_resetwp()
    {
        global $HNResetWP;

        if (!isset($HNResetWP)) {
            $HNResetWP = new ReSetDefaultWP();
        }

        return $HNResetWP;
    }

    HN_resetwp();


endif; // class_exists check

