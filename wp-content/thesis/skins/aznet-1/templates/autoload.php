<?php
/**
 * Name: Require list file.
 * Description: list function for skin.
 * User: Hoang Neo
 */

if (!class_exists('AutoLoadTemplate')) :
    class AutoLoadTemplate
    {

        public function __construct()
        {
            add_action( 'hook_before_ContainerSite',  array( $this, 'getTemplateHeader' ), 1);
            /*------ list content------*/
            //add_action( 'hook_top_ContainerSite',  array( $this, 'getTemplateHeader' ), 2);
            add_action( 'hook_before_ContainerHome',  array( $this, 'getTemplateHome' ), 1);
            add_action( 'hook_before_ContainerPage',  array( $this, 'getTemplatePage' ), 1);
            add_action( 'hook_before_ContainerPageBookingTour',  array( $this, 'getTemplatePageBookingTour' ), 1);

            //load category
            add_action( 'hook_before_ContainerArchive',  array( $this, 'getTemplateArchive' ), 1);
            //load category woocommerce
            add_action( 'hook_before_ContainerWoocommerce',  array( $this, 'getTemplateArchiveWc' ), 1);

            //load category
            add_action( 'hook_before_ContainerNews',  array( $this, 'getTemplateCatNews' ), 1);
            //load single
            add_action( 'hook_before_ContainerSingle',  array( $this, 'getTemplateSingle' ), 1);
            //load single product
            add_action( 'hook_before_ContainerProduct',  array( $this, 'getTemplateProduct' ), 1);

            add_action( 'hook_before_ContainerHotel',  array( $this, 'getTemplateCategoryHotel' ), 1);
            add_action( 'hook_before_ContainerSingleKhachSan',  array( $this, 'getTemplateSingleHotel' ), 1);
            add_action( 'hook_before_ContainerAir',  array( $this, 'getTemplateCategoryAir' ), 1);
            add_action( 'hook_before_ContainerSingleAir',  array( $this, 'getTemplateSingleAir' ), 1);
            add_action( 'hook_before_ContainerCategoryTour',  array( $this, 'gettemplateCategoryTour' ), 1);
            add_action( 'hook_before_ContainerSingleTour',  array( $this, 'gettemplateSingleTour' ), 1);
            add_action( 'hook_before_ContainerPageSearch',  array( $this, 'gettemplateSearch' ), 1);


            /*------ list content------*/
            add_action( 'hook_after_ContainerSite',  array( $this, 'getTemplateFooter' ), 1);
        }
        /**
         * get template
         */
        function getTemplateHeader()
        {
            require(__DIR__. '/header.php');
        }

        function getTemplateFooter()
        {
            require(__DIR__. '/footer.php');
        }

        /**
         * get template
         */
        function getTemplateHome()
        {
            require(__DIR__. '/home.php');
        }
        function getTemplatePage()
        {
            require(__DIR__. '/page.php');
        }
        function getTemplatePageBookingTour(){
            require(__DIR__. '/page-booking-tour.php');
        }
        function getTemplateArchive()
        {
            require(__DIR__. '/archive.php');
        }
 
        function gettemplateSingleTour()
        {
            require(__DIR__. '/single-tour.php');
        }

        
        function gettemplateCategoryTour()
        {
            require(__DIR__. '/category-tour.php');
        }
        function gettemplateSearch()
        {
            require(__DIR__. '/search.php');
        }
 
        function getTemplateCategoryHotel()
 
        {
            require(__DIR__. '/category-khach_san.php');
        }
        function getTemplateSingleHotel()
        {
            require(__DIR__. '/single-khach_san.php');
        }
        function getTemplateCategoryAir()
        {
            require(__DIR__. '/category-ve_may_bay.php');
        }
        function getTemplateSingleAir()
        {
            require(__DIR__. '/single-air.php');
        }

        function getTemplateArchiveWc()
        {
            require(__DIR__. '/archive-wc.php');
        }

        function getTemplateSingle()
        {
            require(__DIR__. '/single.php');
        }

        function getTemplateProduct()
        {
            require(__DIR__. '/single-product.php');
        }


    }

    /**
     * check class is exit
     * call class
     */

    function HN_AutoLoadTemplate()
    {
        global $HNAutoLoadTemplate;

        if (!isset($HNAutoLoadTemplate)) {
            $HNAutoLoadTemplate = new AutoLoadTemplate();
        }

        return $HNAutoLoadTemplate;
    }

    HN_AutoLoadTemplate();

endif;



