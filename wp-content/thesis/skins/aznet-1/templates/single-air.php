 <?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 17/12/2017
 * Time: doinothuetn1008.toannang.xyz/tin-tuc/
 */
 
global $post;
 
?>

<div class="breadcrumbs-wrapper">
<div class="container">
    <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
</div>
</div>
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 cell">
                <div class="content-post">
                    <h1 class="title"><?php the_title(); ?></h1>
                    <div class="blog__post-meta">   
                        <div class="blog__post-date">
                            <span class="month-year"><i class="fas fa-calendar-alt"></i><?php echo get_the_time('d/m/y', $post->ID); ?></span>
                        </div>                     
                        <div class="blog__post-author">
                            <?php $auid = $post->post_author; ?>
                            <span><i class="fas fa-user"></i> <?php echo get_the_author_meta('user_login',$auid); ?></span>
                        </div>
                        <div class="blog__post-category">
                        <i class="fas fa-archive"></i>
                        <?php    $category_detail=get_the_category($post->ID);
                            foreach($category_detail as $cd){
                              echo "<span>".$cd->cat_name."</span>";
                            } ?>
                        </div>
                    </div>
                    <?php the_content(); ?>
                      <div class="fbcomment">
                          <div class="fb-comments" data-href="<?php the_permalink();?>" data-width="100%" data-numposts="5"></div>
                      </div>
                </div>
            </div> 
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 cell">
                <div class="sidebar">
                   <?php dynamic_sidebar( 'widgets'); ?>
                </div>
            </div>
        </div>
    </div>
</section>

