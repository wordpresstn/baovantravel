  <?php $footer = get_field('footer','option'); var_dump( $footer) ; ?>
 <footer id="footer">
    <div class="over-lay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 ">
                <?php if(function_exists('az_box_logo_primary')){ az_box_logo_primary(); } ?>
            </div>
            <div class="col-md-5 col-xs-12 ">
                <?php echo $footer['ten_cty']; ?>
            </div>
            <div class="col-md-5 col-xs-12 ">
                <h3 class="f-h3">Chấp nhận thanh toán</h3>   
                <div class="text-center">
                    <ul class='f-pay'>
                        <li> <a href="#"><img src="/images/tienmat.png"></a></li>
                        <li> <a href="#"><img src="/images/atm.png"></a></li>
                        <li> <a href="#"><img src="/images/visa.png"></a></li>
                        <li> <a href="#"><img src="/images/master.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row f-bottom">
            <div class="col-md-3 col-xs-6">
                <div class="f-col1">
                    <h3>Liên hệ</h3>
                    <div class="f-line"> </div>
                    <p> Địa chỉ: <?php echo $footer['address'] ; ?></p>
                    <p> Điện thoại: <?php echo $footer['hotline'] ; ?></p>
                    <p> Email: <?php echo $footer['email'] ; ?></p>
                    <p> Website: <?php echo $_SERVER['SERVER_NAME'] ; ?></p>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">
                 <div class="f-col2">
                    <h3>Tour trong nước</h3>
                    <div class="f-line"> </div>
                    <ul class="mn-footer">
                        <li><a href="#"> Tour miền tây </a></li>
                        <li><a href="#"> Tour miền trung </a></li>
                        <li><a href="#"> Tour miền nam </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">
                <div class="f-col3">
                    <h3>Tour nước ngoài</h3>
                    <div class="f-line"> </div>
                    <ul class="mn-footer">
                        <li><a href="#"> Tour Lào</a></li>
                        <li><a href="#"> Tour Cumpuchia </a></li>
                        <li><a href="#"> Tour trung quốc</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">
                <div class="f-col4">
                    <h3>Thông tin</h3>
                    <div class="f-line"> </div>
                    <ul class="mn-footer">
                        <li><a href="#"> Tour Lào</a></li>
                        <li><a href="#"> Tour Cumpuchia </a></li>
                        <li><a href="#"> Tour trung quốc</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                 <div class="f-col5">
                    <h3>Kết nối với chúng tôi</h3>
                    <div class="f-line"> </div>
                    
                        <ul class="social">
                            <li > <a href="#" > <img src="/images/twitter.png"> </a></li>
                            <li > <a href="#" > <img src="/images/face.png"> </a></li>
                            <li > <a href="#" > <img src="/images/youtube.png"> </a></li>
                            <li > <a href="#" > <img src="/images/ins.png"></a></li>
                        </ul>
                        <div class="register-form">
                            <p>Đăng ký nhận tin</p>
                            <form method="" >
                                <input class="form-control" name="register-mail" placeholder="Nhập Email của bạn" />
                                <button type="submit" class="btn1">Gửi</button>
                            </form>
                        </div>
                        <div class="search-form">
                            <p>Tim kiếm</p>
                            <form method="" >
                                <input class="form-control" name="register-mail" placeholder="Nhập từ khóa" />
                                <button type="submit" class="btn2"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                </div>
            </div>
        </div>

    </div>
    <div class='copyright'>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <p class="text-center">Copyright @ Bao Ngoc Travel </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="btn-to-top-wrap">
    <a id="btn-to-top" class="circled" href="javascript:void(0);" data-visible-offset="1000"></a>
</div>
