<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 17/12/2017
 * Time: 1:15
 */
?>
<?php
global $post;
?>
<div class="head-child head-product">
    <div class="grid-container">
        <div class="row grid-x">
            <h3 class="title"><?php _e("Menu Food", "azskin") ?></h3>
            <?php if (function_exists('az_box_description_single_product')) az_box_description_single_product(); ?>
        </div>
    </div>
</div>
<section id="container-product">
    <div class="grid-container">
        <div class="row grid-x">
            <div class="single">
                <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post();
                        wc_get_template_part( 'content', 'single-product' );
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</section>

