<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/28/2018
 * Time: 4:24 PM
 */

 
global $post ;
function booking_tour_wp_enqueue_script() {
    wp_enqueue_style( 'booking', home_url(). '/assets/css/booking.css' );
}
add_action( 'wp_enqueue_scripts', 'booking_tour_wp_enqueue_script' );
 

if(!empty($_GET['tourid'])){
    $tourID = $_GET['tourid'];
    $post = get_post($tourID) ;
     
}

if(count($post)) {
 
 
?>

<div class="breadcrumbs-wrapper">
    <div class="container">
        <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
    </div>
</div>
 
<main role="main" class="chitiettour" >
    <div class="content-page">
        <div class="container">
            <div class="row tour-info" style="margin:30px 0  60px 0 ; ">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="t-info-left col-md-4 col-sm-12 left tour-info-mgr mg-bot10">
                            <div class="tour-info-left-frame">
                                <img src="<?php echo get_the_post_thumbnail_url( $post->ID , 'full' );  ?>" alt="<?php the_title(); ?> " class="img-responsive " style="width:100%;">
                                <div style="margin-top: 10px">
                                    <div class="f-left tour-info-left">
                                        <div style="border-right: none !important; text-align: center; margin: 0px !important; height: 50px; line-height: 50px">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;Số ngày:
                                            <span><?php echo get_field('trip_thoigian', $post->ID) ?></span>
                                        </div>
                                    </div>
                                    <div class="f-right tour-info-right"><?php echo  number_format( get_field('trip_giakm', $post->ID))  ?><span> đ</span></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 right tour-info-right-frame">
                            <div class="row tour-info-right-frame1">
                                <div class="col-md-12">
                                    <div style="font-weight: bold; font-size: 17.5px; margin-bottom: 10px; line-height: 22px;">
                                       <?php the_title(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row tour-info-right-frame2">
                                <div class="col-md-12">
                                    <div class="row" style="margin-bottom: 16px; margin-top: 15px">
                                        <div class="col-md-3 col-sm-3 col-xs-6 mg-bot5">Mã tour:</div>
                                        <div class="col-md-9 col-sm-9 col-xs-6"><?php echo get_field('trip_sku') ?></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 16px; margin-top: 15px">
                                        <div class="col-md-3 col-sm-3 col-xs-6 mg-bot5">Ngày khởi hành:</div>
                                        <div class="col-md-3 col-sm-4 col-xs-6"><?php echo get_field('trip_date_begin') ?></div>
                                        <div class="col-md-3 col-sm-5 col-xs-12"><i class="fa fa-calendar"></i>&nbsp;&nbsp;<a href="#">Ngày khác</a></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 31px; margin-top: 15px">
                                        <div class="col-md-3 col-sm-3 col-xs-6 mg-bot5">Nơi khởi hành:</div>
                                        <div class="col-md-3 col-sm-4 col-xs-6 mg-bot5"> 
                                            <?php $terns = get_the_terms(  $post->ID, 'destination' ) ; 
                                           if(!empty($terns)) { foreach($terns as $tern){ echo $tern->name .'   ' ; } } ?>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6">Số chỗ còn nhận:</div>
                                        <div class="col-md-3 col-sm-2 col-xs-6">6</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="chuy">
                                    <span style="display: table-cell; vertical-align: middle; line-height: 18px; color: #cd2c24;">
                                        Khách nữ từ 55 tuổi trở lên, khách nam từ 60 tuổi trở lên đi tour một mình và khách mang thai trên 4 tháng (16 tuần) vui lòng đăng ký tour trực tiếp tại văn phòng của Vietravel. Không áp dụng đăng ký tour online đối với khách từ 70 tuổi trở lên
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-booking">
        <div class="container">
        <?php echo do_shortcode('[gravityform id="1" name="Booking Tour" title="false" description="false" ajax="false"]');?>
        </div>
    </div>
</main>
<?php   }else{
    echo '<p class="text-center" style="padding:30px;">Không tìm thấy Tour</p>';
}   ?>