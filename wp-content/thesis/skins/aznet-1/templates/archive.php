<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 17/12/2017
 * Time: 1:15
 */

global $post;
?>
<?php if ( have_posts() ) : ?>
     <div class="head-child head-archive-wc">
        <div class="breadcrumbs-wrapper">
            <div class="container">
                 <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
            </div>
        </div>
    </div>
    <div class="head-child head-archive-wc">
        <div class="container">
            <div class="col-xl-12">
                <h1 class="title"><?php the_archive_title(); ?></h1>
                <div class="description">
                    <?php the_archive_description(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="archive-news">
        <div class="container">
            <div class="row">
                    <?php
                    while ( have_posts() ) : the_post();
                        ?>
                        <div id="post-<?php echo $post->ID; ?>" class="col-xl-3 col-sm-6 col-xs-12 post-cat">
                            <div class="blog__item grid-x">
                                <figure class="small-4 cell">
                                    <a href="<?php echo get_permalink($post->ID); ?>">
                                        <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'thumbnail'); ?>" alt="<?php the_title(); ?>">
                                    </a>
                                    <div class="blog__post-date">
                                        <span class="days"><?php echo get_the_time('d', $post->ID); ?></span>
                                        <span class="month-year"><?php echo get_the_time('m/y', $post->ID); ?></span>
                                    </div>
                                </figure>
                                <div class="blog__entry small-8 cell">
                                    <h3 class="blog__entry__title">
                                        <a href="<?php echo get_permalink($post->ID); ?>"><?php the_title() ?></a>
                                    </h3>
                                    <div class="blog__post-meta">
                                       
                                        <div class="blog__post-author">
                                            <?php $auid = $post->post_author; ?>
                                            <span><i class="fas fa-user"></i> <?php echo get_the_author_meta('user_login',$auid); ?></span>
                                        </div>
                                        <div class="blog__post-category">
                                        <i class="fas fa-archive"></i>
                                         <?php      
                                            $category_detail=get_the_category($post->ID);
                                            foreach($category_detail as $cd){
                                                echo "<span>".$cd->cat_name."</span>";
                                                } ?>
                                           
                                        </div>
                                    </div>
                                    <div class="excerpt_content"><?php echo az_get_excerpt_content($post->ID,40); ?></div>
                                     
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    ?>
                    
                    <?php wp_pagenavi(); ?>        
            </div>
        </div>
    </div>
<?php endif; ?>