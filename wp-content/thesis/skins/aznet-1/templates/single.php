<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 17/12/2017
 * Time: 1:15
 */
?>
<?php
global $post;
$thum = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "full");
?>
 
<div class="breadcrumbs-wrapper">
<div class="container">
    <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
</div>
</div>
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 cell">
                <div class="content-post">
                    <h1 class="title"><?php the_title(); ?></h1>
                    <div class="blog__post-meta">   
                        <div class="blog__post-date">
                            <span class="month-year"><i class="fas fa-calendar-alt"></i><?php echo get_the_time('d/m/y', $post->ID); ?></span>
                        </div>                     
                        <div class="blog__post-author">
                            <?php $auid = $post->post_author; ?>
                            <span><i class="fas fa-user"></i> <?php echo get_the_author_meta('user_login',$auid); ?></span>
                        </div>
                        <div class="blog__post-category">
                        <i class="fas fa-archive"></i>
                        <?php    $category_detail=get_the_category($post->ID);
                            foreach($category_detail as $cd){
                                echo "<span>".$cd->cat_name."</span>";
                                } ?>
                            
                        </div>
                    </div>
                    <?php the_content(); ?>
                    <div class="blog__post-tag">
                        <i class="fas fa-tag"></i>
                        
                        <?php    $tags = get_the_tags($post->ID);
                               if($tags){
                                  echo "<span> Tags :</span>";
                                   foreach ($tags as $tag) {?>
                                      <a href="<?= get_tag_link($tag->term_id); ?>" title="<?= $tag->name; ?>"><?= $tag->name; ?> </a>
                                   <?php }
                               }
                           ?>
                            
                        </div>
                        <div class="post_next_prev_link">
                            <?php
                                $prev_post = get_previous_post();
                                
                                if (!empty( $prev_post )): ?>
                                 
                                <a class="prevlink" href="<?php echo get_permalink($prev_post->ID); ?>"><span>Bài trước</span><?php echo $prev_post->post_title ?></a>
                                <?php endif ?>
                                <?php
                                $next_post = get_next_post();
                                if (!empty( $next_post )): ?>
                                <a class="nextlink" href="<?php echo get_permalink($next_post->ID); ?>"><span>Bài sau</span><?php echo $next_post->post_title ?></a>
                                <?php endif ?>
                        </div>
                    <div class="fbcomment">
                        <div class="fb-comments" data-href="<?php the_permalink();?>" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 cell">
                <div class="sidebar">
                       <?php   dynamic_sidebar( 'widgets'); ?>                   
                </div>
            </div>
        </div>
    </div>
</section>

