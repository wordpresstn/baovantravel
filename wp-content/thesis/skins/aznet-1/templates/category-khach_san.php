 <?php
/**
 *  
 *  
 */
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
 $posts = get_posts([
	  'post_type' => 'khach_san',
	  'post_status' => 'publish',
	  'posts_per_page  ' =>2,
      'paged' => $paged,
	]
);

 
?> 
<?php $footer = get_field('footer','option');?> 
<link id="hotel-css" rel="stylesheet" type="text/css" href="<?php echo home_url().'/assets/css/hotel.css' ; ?>" >   
<?php if ( count($posts) ) : ?>
    <div class="head-child head-archive-wc">
        <div class="breadcrumbs-wrapper">
			<div class="container">
        		 <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
        	</div>
        </div>
    </div>
    <div class="container">
        <h1 class="title"><?php the_archive_title(); ?></h1>
        <div class="description">
            <div class="fb-like" data-href="<?php echo $footer['social']['facebook']?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        	<?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings($pid); endif; ?>
            <?php the_archive_description(); ?>
        </div>
    </div>
    <div class="category-hotel">
        <div class="container">
            <div class="row">  
                <?php  foreach ( $posts as $post ) :  ?>
                    <div id="post-<?php echo $post->ID; ?>" class="col-lg-4 col-md-6 col-sm-6     post-cat">
                        <figure class=" ">
                            <a href="<?php echo get_permalink($post->ID); ?>">
                                <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php the_title(); ?>">
                            </a>
							<div class="hotel-start"> 
                        		<?php 
                        			$starts = get_field('hotel_start' ,  $post->ID )    ; 
                        			for( $i=0 ; $i < $starts ; $i++ ){ ?>
                        				<i class="fas fa-star"></i>  
                        		<?php } ?> 
                        	 </div>
                             <div class="blog__entry  ">
                             	 
                                <h3 class="blog__entry__title">
                                    <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title ; ?></a>
                                </h3>
                                <p class="price-hotel"> Giá từ: <span> <?php echo number_format(get_field('hotel_price', $post->ID)) ;?> đ  </span></p>
                                
                        	</div> 
                        	
                        </figure>   
                    </div>
                <?php
                endforeach;
                ?>
                <?php wp_pagenavi(); ?>        
            </div>
        </div>
    </div>
<?php endif; ?>