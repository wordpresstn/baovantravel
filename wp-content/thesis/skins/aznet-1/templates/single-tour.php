 <?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 17/12/2017
 *  
 */
 
global $post;
 
?>

<div class="breadcrumbs-wrapper">
<div class="container">
    <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
</div>
</div>
<section id="single-tour">
    <div class="container">
        <div class="row">
            <div class="  col-lg-5 col-md-5 col-sm-12 ">
                 <img src="<?php echo get_the_post_thumbnail_url() ; ?>" class="img-responsive" style="width:100% ;" >
            </div> 
            <div class="  col-lg-7 col-md-12 col-sm-12 ">
                <h1 class="title-tour"><?php the_title();  ?></h1>
                
                     <div class="description">
                        <div class="fb-like" data-href="<?php echo $footer['social']['facebook']?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                        <?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings($pid); endif; ?>
                        <?php the_archive_description(); ?>
           
                        <?php echo $post->post_excerpt ; ?> 
                        <div class="info-tour">
                           <div class="row">
                                <div class="col-md-6">

                                    <p >Mã tour: <span><b> <?php echo get_field('trip_sku') ; ?></b> </span> </p>
                                    <p >Khởi hành: <span>
                                        <?php
                                             $terns = get_the_terms(  $post->ID, 'destination' ) ; 
                                             if(!empty($terns)){
                                                 foreach($terns as $tern){ echo $tern->name .', ' ; }   
                                             }
                                            
                                        ?> </span> </p>
                                    <p >Thời gian: <span> <?php echo get_field('trip_thoigian') ;?></span> </p>
                                    <p >Ngày khởi hành: <span> <?php echo get_field('trip_date_begin') ;?></span> </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="price-tour text-right">   <del><?php echo number_format  (get_field('trip_giathuong')   )  ?> <sup>đ</sup></del>   </p>
                                    <p class="price-tour-sale text-right">Giá từ: <span> <?php echo number_format(get_field('trip_giakm')   )  ?> </span> <sup>đ</sup></p>
                                    <p class="text-right">Số chổ:   <span><?php echo get_field('trip_socho')?></span> </p>
                                </div>
                                <div class="col-md-12">
                                   <button class="btn pull-left  btn-danger"><a href="/dat-tour/?tourid=<?php the_ID() ?>  ">Đặt ngay</a></button>
                                </div>
                            </div>
                    </div>
                    </div> 
                    
            </div> 
            
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-9 ">
                <div class="tab-muilty">
                    <ul class="nav nav-tabs">
                      <li class=""><a data-toggle="tab" href="#menu0">Tổng quan</a></li>
                      <li class=""><a data-toggle="tab" href="#menu1">Lịch khởi hành</a></li>
                      <li class=""><a data-toggle="tab" href="#menu2">Lịch trình</a></li>
                      <li class="active"><a data-toggle="tab" href="#menu3">Ghi chú</a></li>
                        
                    </ul>
                    <div class="tab-content">
                        <div id="menu0" class="tab-pane fade active in">
                            <?php echo get_field('trip_tongquan') ; ?>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                          <?php echo get_field('trip_lichtrinh') ; ?>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                          <?php echo get_field('trip_lichkhoihanh') ; ?>
                        </div>
                        <div id="menu3" class="tab-pane fade ">
                            <?php echo get_field('trip_ghichu') ; ?>            
                       </div>
                       
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-3">
                 <div class="sidebar">
                   <?php dynamic_sidebar( 'widgets'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
 
