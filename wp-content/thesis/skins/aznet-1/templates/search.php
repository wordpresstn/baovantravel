<?php
global $post;
?>
<div class="head-child head-archive-wc">
    <div class="breadcrumbs-wrapper">
        <div class="container">
             <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
        </div>
    </div>
</div>
 <div class="archive-tour  categries-tour">
       <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h2 class="title-h2"> Tìm kiếm</h2>
                </div>
                <?php if ( have_posts() ) : ?>

                <div class="col-md-12 col-sm-12 col-lg-12  col-xs-12 ">
                    <div class="row">
                    <?php   while ( have_posts() ) : the_post();  ?>  
                        <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                            <div class="box-tour">
                                <a href="<?php echo get_permalink($post->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php the_title(); ?>" class="img-responsive"  /></a>
                                <div class="info">
                                    <h5><a href="<?php echo get_permalink($post->ID); ?>" ><?php the_title(); ?></a></h5>
                                    <p class="price-tour"> <?php echo number_format(get_field('trip_giakm')   )  ?>  VND</p>
                                    <div class="info-more" >
                                        <p class="info-time">Thời gian:  <?php echo get_field('trip_thoigian') ;?></p>
                                        <p class="info-begin">Điểm khởi hành: <?php $terns = get_the_terms(  $post->ID, 'destination' ) ; 
                                        if(!empty($terns)) {
                                             foreach($terns as $tern){ echo $tern->name   ;    }
                                       }  
                                         ?> </p>
                                        <p class="info-bus">Phương tiện:  <?php echo get_field('trip_phuongtien', $post->ID)?></p>
                                        <?php $star = get_field('trip_hotel', $post->ID);  ?>
                                        <p class="info-hotel">Khách sạn: 
                                           <?php 
                                            for($i=0 ; $i < $star  ; $i++){
                                                echo '<span><i class="fas fa-star"></i></span>'; 
                                            }
                                           ?>
                                        </p>
                                        <button class="btn pull-left  btn-danger"><a href="/dat-tour/?tourid=<?php echo $post->ID ?>  ">Đặt ngay</a></button>
                                        <button onclick="window.location.href='<?php echo get_permalink($post->ID); ?>'" class="btn pull-right  btn-info">Chi tiết</button>          
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile ; ?>
                        <?php wp_pagenavi(); ?>   
                     </div>
                </div>
            <?php endif ; ?>
            </div>
          </div>
      </div>
 