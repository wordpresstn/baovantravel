<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 17/12/2017
 * Time: 1:14
 */
global $post;

if ( have_posts() ) : ?>
    <div class="head-child head-archive-wc">
        <div class="grid-container">
            <div class="row grid-x">
                <h1 class="title"><?php the_archive_title(); ?></h1>
                <div class="description">
                    <?php the_archive_description(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="archive-product">
        <div class="grid-container">
            <?php
            /**
            * woocommerce_before_shop_loop hook.
            *
            * @hooked wc_print_notices - 10
            * @hooked woocommerce_result_count - 20
            * @hooked woocommerce_catalog_ordering - 30
            */
            do_action( 'woocommerce_before_shop_loop' );

            woocommerce_product_loop_start();
            woocommerce_product_subcategories();

            while ( have_posts() ) : the_post();

                /**
                 * woocommerce_shop_loop hook.
                 *
                 * @hooked WC_Structured_Data::generate_product_data() - 10
                 */
                do_action( 'woocommerce_shop_loop' );


                wc_get_template_part( 'content', 'product' );

            endwhile; // end of the loop.

            woocommerce_product_loop_end();


            /**
            * woocommerce_after_shop_loop hook.
            *
            * @hooked woocommerce_pagination - 10
            */
            do_action( 'woocommerce_after_shop_loop' );
        ?>
            <?php wp_pagenavi(); ?>
        </div>
    </div>
<?php

elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) :
    ?>
    <div class="archive-product">
        <div class="container">
        <?php
        /**
        * woocommerce_no_products_found hook.
        *
        * @hooked wc_no_products_found - 10
        */
        do_action( 'woocommerce_no_products_found' );

        ?>
        </div>
    </div>
    <?php
endif;
