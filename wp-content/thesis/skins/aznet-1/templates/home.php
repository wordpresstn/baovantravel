<sectiom class="homa-page">
    <div class="banner-top">
        <div class="container">
            <div class="row">
             <?php if( have_rows('home-services','option') ):   
                    while ( have_rows('home-services','option'))  : the_row() ;
                        $category = get_sub_field('category-url');      
                        $name = get_sub_field('service-name'); 
                        $image = get_sub_field('service-image');   ?>
                        <div class="col-md-3 col-xs-6">
                            <div class="banner-img">
                                <a href="<?= get_term_link($category->term_id) ?>">  <img src="<?php  print_r($image) ?>" alt="<?= $name ?>" /> </a>
                           </div>
                        </div>
            <?php endwhile;endif; ?>
            </div>
        </div>
    </div>
    
    <div class="categries-tour">
       <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h2 class="title-h2"> Tour đang hot</h2>
                </div>
                <div class="col-md-12 col-lg-3 col-sm-12 col-xs-12 ">
                    <div class="banner-tour">
                        <a href="#"><img src="/images/banner-tour.png" class="img-responsive"  /></a>
                    </div>
                </div> 
                <div class="col-md-12 col-sm-12 col-lg-9  col-xs-12 ">
                    <div class="row">
                        <?php 
                        
                        $posts = get_posts(array(
                            'numberposts'   => 6,
                            'post_type'     => 'trip',
                            'meta_query'    => array(
                                array(
                                    'key'   => 'trip_hot',
                                    'value' => '1',          
                                )
                        )));
                        if(count( $posts )):
                            foreach( $posts as $post ):
                        ?>
                        <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12">
                            <div class="box-tour">
                                <a href="<?php echo get_permalink($post->ID) ; ?>"><img src="<?php echo get_the_post_thumbnail_url( $post->ID , 'full' );  ?>" class="img-responsive"  /></a>
                                <div class="info">
                                    <h5><a href="<?php echo get_permalink($post->ID) ; ?>" ><?php echo $post->post_title ;  ?></a></h5>
                                    <p class="price-tour"><?php echo number_format(get_field('trip_giakm', $post->ID))  ; ?> VND</p>
                                    <div class="info-more" >
                                        <p class="info-time">Thời gian: <?php echo get_field('trip_thoigian', $post->ID ) ; ?></p>
                                        <p class="info-begin">Điểm khởi hành: 
                                            <?php 
                                            $terns = get_the_terms(  $post->ID, 'destination' ) ;
                                            if(count( $terns)) {
                                                foreach($terns as $tern){ 
                                                    echo $tern->name .', ' ; 
                                                } 
                                            } 
                                            ?> 
                                         </p>
                                        <p class="info-bus">Phương tiện: <?php echo get_field('trip_thoigian', $post->ID ) ; ?></p>
                                        <p class="info-hotel">Khách sạn: 
                                        <?php 
                                        $star = get_field('trip_hotel', $post->ID);
                                        for($i=0 ; $i < $star  ; $i++){ ?>
                                            <span><i class="fas fa-star"></i></span> ; 
                                        <?php }  ?>
                                         </p>
                                        <button class="btn pull-left  btn-danger" tabindex="0"><a href="/dat-tour/?tourid=<?php echo $post->ID ?>  " tabindex="0">Đặt ngay</a></button>
                                        <button class="btn pull-right  btn-info" tabindex="0"><a  href="<?php echo get_permalink($post->ID) ; ?>" tabindex="0">Chi tiết</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <?php endforeach ; ?>
                        <?php endif ; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h2 class="title-h2"> Tour trong nước</h2>
                    <span class="view-all"><a href="#">Xem tất cả >> </a></span>
                </div>
                <div class=" overflow-hide">
                    <div class="slick-tour-location cate-tour">
                        <?php if(function_exists('az_box_list_tour_local')){ az_box_list_tour_local(); } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h2 class="title-h2"> Tour nước ngoài</h2>
                    <span class="view-all"><a href="#">Xem tất cả >> </a></span>
                </div>
                <div class=" overflow-hide">
                    <div class="slick-tour-word cate-tour">
						<?php if(function_exists('az_box_list_tour_word')){ az_box_list_tour_word(); } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="why"   >
        <div class="over-lay"> </div>
        <div class="container">
            <div class="row">
               <?php 
                    $custom_field = get_field('slider_box_trang_chu','option'); 
                    echo $custom_field['contact-info'] ; 
                ?> 
            </div>
        </div>
    </div>
    <div class="home-news"   >
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h2 class="title-h2"> Thông tin du lịch</h2>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                             <?php if(function_exists('az_box_list_news_1')){az_box_list_news_1();} ?>  
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <!---slick news-->
                            <div class="slide-news-home">
                                <?php if(function_exists('az_box_list_news')){az_box_list_news();} ?>  
                            </div>
                            <!---end slick-->
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <h2 class="title-h2"> Cảm nhận khách hàng</h2> 
                    <div class="row">
                        <div class="col-md-10 offset-md-1 col-xs-12">
                            <div class="feel ">
                                <div class=" slide-feel">
                                     <?php if(function_exists('az_box_list_camnhan')){az_box_list_camnhan();} ?>  
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 