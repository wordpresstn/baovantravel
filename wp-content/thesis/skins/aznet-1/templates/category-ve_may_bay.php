 <?php
/**
 *  
 *  
 */

 $posts = get_posts([
	  'post_type' => 'ticket_air',
	  'post_status' => 'publish',
	  'posts_per_page  ' =>30
	]
);
 

?> 
<?php $footer = get_field('footer','option');?> 
<?php if ( count($posts) ) : ?>
    <div class="head-child head-archive-wc">
        <div class="breadcrumbs-wrapper">
			<div class="container">
        		 <?php if(function_exists('az_box_breadCrumbs')){az_box_breadCrumbs();}?>
        	</div>
        </div>
    </div>
    <div class="container">
        <h1 class="title"><?php the_archive_title(); ?></h1>
        <div class="description">
        	<div class="fb-like" data-href="<?php echo $footer['social']['facebook']?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        	<?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings($pid); endif; ?>
            <?php the_archive_description(); ?>
        </div>
    </div>
    <div class="category-air">
        <div class="container">
            <div class="row">  
                <?php  foreach ( $posts as $post ) :  ?>
                    <div id="post-<?php echo $post->ID; ?>" class="col-lg-4 col-md-6 col-sm-6     post-cat">
                        <figure class=" ">
                            <a href="<?php echo get_permalink($post->ID); ?>">
                                <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php the_title(); ?>">
                            </a>
							 
                             <div class="blog__entry  ">
                             	 
                                <h3 class="blog__entry__title">
                                    <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title ; ?></a>
                                </h3>
                              
                                
                        	</div> 
                        	
                        </figure>   
                    </div>
                <?php
                endforeach;
                ?>
                <?php wp_pagenavi(); ?>        
            </div>
        </div>
    </div>
<?php endif; ?>
<style>
	.post-cat{
		margin-bottom:15px;
	}
	 .blog__entry {
	    position: absolute;
	    bottom: 0;
	    color: #fff;
	    width: 100%;
	    text-align: center;
	    background: -webkit-linear-gradient(top, rgba(47, 39, 39, 0), rgb(0, 0, 0));
	}
	.blog__entry__title {
	    font-size: 20px;
	}
	.blog__entry__title a {
	    color: #fff;
	}
	.category-air figure img:hover {
	    transform: scale(1.05);
	    transition: 1s;
	}
	.category-air img{
		overflow: hidden;
		transition: 1s;
	}
	@media (min-width: 1280px){
		.post-cat figure {
		   height: inherit;
		   max-height:357px;
		}
	}

</style>