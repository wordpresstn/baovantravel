<?php

/**
* desktop header file
*/

?>
 <?php $footer = get_field('footer','option');?>
<header>   
    <div id="main-header" class="nav-menu">
        <div class="container">
            <div class="row">
                <div class="col-xl-1 padding-0">
                    <?php if(function_exists('az_box_logo_primary')){ az_box_logo_primary(); } ?>               
                </div>
                <div class="col-xl-9">
                    <div id="mainmenu">
                    <?php if(function_exists('az_box_menu_primary')){ az_box_menu_primary(); } ?>
                    </div>
                    <?php if(function_exists('az_box_menu_primary')){ az_box_menu_primary(); } ?>
                    <a class="icon-menu-mobile" href="#mainmenu">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </a>
                </div>
                 <div class="col-xl-2">
                   <div class="hotline">
                        <img src="/images/icon.png">
                        <div>
                            <span>Chăm sóc khách hàng</span>
                            <span><strong> <?php echo $footer['hotline'];?> </strong></span>
                         </div>
                      
                   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-home-page"> 
        <?php if (function_exists('az_box_slider_home')){ az_box_slider_home(); } ?> 
        <!--------Filtẻr-------->
        <div id="form-search" class="">
        <div class="form-search-frame">
            <div id="tabs-container">
                <ul class="tabs-menu nav-tabs">
                    <li class=" active"><a href="#tab-1"  data-toggle="tab"  >TÌM TOUR</a> </li>
                    <li class=""><a href="#tab-2" >TRA CỨU BOOKING</a> </li>
                </ul>
            </div>
            <div class="tab">
                <div id="tab-1" class="tab-content active"  >
                    <form action="/" method="get">  
                        <input type="hidden" name="s"> 
                        <div class="row form-group">
                            <div class="col-md-6 col-sm-4 col-xs-6 mg-bot15">
                                <label>Nơi khởi hành</label>
                                <div>  
                                    <select class="form-control input-md" id="group_id" name="diemkhoihanh">
                                        <option value=0 >Tất cả</option> 
                                         <?php 
                                            $terms = get_terms( array(
                                                'taxonomy' => 'diem_khoi_hanh',
                                                'hide_empty' => false,
                                            ) ); var_dump($terms );
                                            if(!empty( $terms )){
                                                foreach( $terms as $tern ){
                                                ?>
                                                <option value="<?php echo $tern->term_id ; ?>" <?php if($_GET['diemkhoihanh'] == $tern->term_id) echo 'selected'; ?>  > <?php echo $tern->name ; ?></option>
                                                <?php
                                                }
                                            }
                                         ?>
                                    </select>
                                   
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4 col-xs-6  mg-bot15">
                                <label>Loại tour</label>
                                <div>
                                    <select class="form-control input-md" id="TourTypeId" name="tourtype">
                                        <option value="trongnuoc" <?php if($_GET['tourtype']=='trongnuoc') echo 'selected'; ?> >Tour trong nước</option>
                                        <option value="nuocngoai" <?php if($_GET['tourtype']=='nuocngoai') echo 'selected'; ?>>Tour nước ngoài</option>
                                    </select>
                                </div>
                            </div>
                   
                            <div class="col-md-6 col-sm-4 col-xs-6 mg-bot15">
                                <label>Nơi đến</label>
                                <div>
                                     <select class="form-control input-md" id=" " name="noiden">
                                        <option value="0">Tất cả</option>
                                        <?php 
                                            $terms = get_terms( array(
                                                'taxonomy' => 'destination',
                                                'hide_empty' => false,
                                            ) );
                                            if(!empty( $terms )){
                                                foreach( $terms as $tern ){
                                                ?>
                                                <option value="<?php echo $tern->term_id ?>"> <?php echo $tern->name ; ?></option>
                                                <?php
                                                }
                                            }
                                         ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4 col-xs-6  mg-bot15">
                                <label>Ngày khởi hành</label>
                                <div style="position: relative;">
                                    <input class="form-control date-form" value="<?php echo $_GET['ngaykhoihanh']; ?>" id="departure_date" name="ngaykhoihanh" placeholder="1/06/2018" type="text" value="">
                                    <span class="i-calendar"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>
                       
                            <div class="col-md-6 col-sm-4 col-xs-6">
                                <label>Giá</label>
                                <div>
                                    <select class="form-control input-md" id="priceID" name="price">
                                        <option value="0" <?php if($_GET['price']=='0') echo 'selected'; ?>>Tất cả</option>
                                        <option value="1" <?php if($_GET['price']=='1') echo 'selected'; ?>>Dưới 1 Triệu</option>
                                        <option value="2" <?php if($_GET['price']=='2') echo 'selected'; ?>>  1 - 2 Triệu</option>
                                        <option value="3" <?php if($_GET['price']=='3') echo 'selected'; ?>>  2 - 5 Triệu</option>
                                        <option value="4" <?php if($_GET['price']=='4') echo 'selected'; ?>>  5 - 10 Triệu</option>
                                        <option value="5" <?php if($_GET['price']=='5') echo 'selected'; ?>>  10 - 15 Triệu</option>
                                        <option value="6" <?php if($_GET['price']=='6') echo 'selected'; ?>>  15 - 20 Triệu</option>
                                        <option value="7" <?php if($_GET['price']=='7') echo 'selected'; ?>>  Trên 20 Triệu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4 col-xs-6">
                                <label>Dòng tour</label>
                                <div>
                                    <select class="form-control" id="DongTour" name="dongtour"><option value="0" >Tất cả</option>
                                        <option value="all" <?php if($_GET['tourtype']=='all') echo 'selected'; ?>>Tất cả</option>
                                        <option value="caocap" <?php if($_GET['tourtype']=='caocap') echo 'selected'; ?>>Cao cấp</option>
                                        <option value="tieuchuan" <?php if($_GET['tourtype']=='tieuchuan') echo 'selected'; ?>>Tiêu chuẩn</option>
                                        <option value="tietkiem" <?php if($_GET['tourtype']=='tietkiem') echo 'selected'; ?>>Tiết kiệm</option>
                                        <option value="giatot" <?php if($_GET['tourtype']=='giatot') echo 'selected'; ?>>Giá Tốt</option>
                                    </select>
                                </div>
                                 
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6 col-sm-4 col-xs-6">
                                <label>Tình trạng</label>
                                <div>
                                    <select class="form-control"  name="stock">
                                        <option value="all" <?php if($_GET['stock']=='all') echo 'selected'; ?>>Tất cả</option>
                                        <option value="1" <?php if($_GET['stock']=='1') echo 'selected'; ?> >Còn chỗ</option>
                                        <option value="0" <?php if($_GET['stock']=='0') echo 'selected'; ?>>Hết chỗ</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4 col-xs-6">
                                <label>&nbsp;</label>
                                <div>
                                    <button type="submit" class="btn btn-md btn-default" >
                                        <div class="f-left">
                                            <div class="f-left" style="margin-left:3px"><span>Tìm kiếm <img src="/images/icon2.png"></span></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>              
                </div>
                <div id="tab-2" class="tab-content"  >
                    <form action="" method="get">
                        <input name=" " type="hidden" >                       
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <div class="content-trabooking">Quý khách vui lòng nhập số booking vào ô trống bên dưới và nhấn nút "Tìm kiếm"</div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-7 col-sm-9 col-xs-12 input-sb">
                                    <input class="form-control" id="pin_code" name="pin_code" type="text" placeholder="Số booking">
                                </div>
                                <div class="col-md-5 col-sm-3 col-xs-12 btn-sb">
                                    <button type="submit" class="btn btn-md btn-default">
                                         <div class="f-left">
                                           
                                            <div class="f-left" style="margin-left:10px"><span>Tìm kiếm <img src="/images/icon2.png"></span></div>
                                        </div>    
                                    </button>
                                </div>
                            </div>
                    </form>                
                </div>
            </div>
        </div>
    </div>
        <!--------end filter------->

</header>

    