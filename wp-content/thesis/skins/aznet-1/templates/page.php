<?php
/**
 * Created by PhpStorm.
 * User: vancuong
 * Date: 14/12/2017
 * Time: 23:55
 */
?>
<?php
global $post;
    $thum = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "full");
?>
<div class="head-child head-archive-wc">
    <div class="container">
        <div class="col-xl-12">
            <h1 class="title"><?php the_title(); ?></h1>
        </div>
    </div>
</div>
<main role="main" >
    <div class="container">
        <div class="content-page">
            <?php if(is_page(12)) :
                $location = get_field('google_map');
                if( !empty($location) ):
                ?>
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-xs-12">
                        <?php  the_content(); ?>
                    </div>
                    <div class="col-xl-6 col-md-6 col-xs-12">
                    <div class="acf-map">
                        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                    </div>

                    </div>
                </div>
                
                <?php endif; 
            else :?>
            <?php
             the_content();
            endif;
            ?>
        </div>
    </div>
</main>